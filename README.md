To install type- (make sure virtualenv is installed)
1) virtualenv venv
2) source venv/bin/activate
3) pip install requirements.txt
4) python manage.py runserver
5) Go to http://localhost:8000/api/search_tweets/?&q=metoo

Due to shortage of time, I am explaining what ill be 
doing with the rest of the tasks for the rest of the tasks

1) for exact/partial matching strings, I would use regex and differentiate between exact or partial through a key in query paramters. Same can be done with starts with, endswiths
2) For integers, I would specify which key for integer, and check if it is integer, and use same search_by_key_value functionality
3) For sorting, same, specify key, and sort according to that, the flattened dict will help here
4) For grouping, I am not clear what is exactly required so I have left it out, but we can discuss it later on in the process once clear.