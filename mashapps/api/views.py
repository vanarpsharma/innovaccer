# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import requests
import flatdict
import dateparser
import datetime, time
import oauth2 as oauth

from copy import deepcopy
from rest_framework.decorators import api_view
from rest_framework.response import Response


class TwitterRequest():
    consumer_key = "O4KdpYDTZYEkQj4uDhJxtshq0"
    consumer_secret = "9ZUuJHiplVnD0znw7hIpApA97cy3VJjs0D6l7WwCiUGXqlEeFH"
    access_token = "3309068633-ZQTknBYYbsyCCOwqzn0zuXtW60jYBsTw9OXhfnP"
    access_token_secret = "qgkvcUVb0EL1z2IrjY6wx88vVC8q6XwDjfCebm8keLi5J"
    endpoint = "https://api.twitter.com/1.1/search/tweets.json?q={query}"

    def __init__(self):
        consumer = oauth.Consumer(key=self.consumer_key, secret=self.consumer_secret)
        access_token = oauth.Token(key=self.access_token, secret=self.access_token_secret)
        self.client = oauth.Client(consumer, access_token)

    def get_endpoint(self, query):
        return self.endpoint.format(query=query)

    def make_request(self, request):
        return self.client.request(request)

@api_view(['GET'])
def search_tweets(request):
    if request.method == 'GET':
        query = request.query_params['q']
        # using .get to handle non-existent parameters gracefully
        search_key = request.query_params.get('key')
        search_value = request.query_params.get('value')
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')
        # wrapped oauth request functionality in a Class
        request_instance = TwitterRequest()
        request_endpoint = request_instance.get_endpoint(query)
        response, data = request_instance.make_request(request_endpoint)
        data = json.loads(data)
        # The idea is to flatten the list, thereby allowing a strategy to query and find exact or partial matching keys
        data = flatten_list(data['statuses'])
        if bool(search_key):
            data = search_by_key_or_value(data, search_key, 1)
        if bool(search_value):
            data = search_by_key_or_value(data, search_value, 0)
        if bool(date_from):
            data = search_by_date_range(data, date_from, date_to)
        # have included flattened list purposely so as to make it clear what has been dne
        return Response(data)  

def flatten_list(target_list):
    new_list = []
    for item in target_list:
        unit = dict(flat=None, original=item)
        unit['flat'] = flatdict.FlatDict(item)
        new_list.append(unit)
    return new_list

def search_by_key_or_value(d, search_term, key):
    result_set = []
    if key:
        for item in d:
            for key, value in item['flat'].iteritems():
                if search_term in key:
                    result_set.append(item['original'])
                    break
    else:
        for item in d:
            for key, value in item['flat'].iteritems():
                try:
                    if search_term in value:
                        result_set.append(item['original'])
                        break
                except:
                    pass
    return result_set

def search_by_date_range(d, date_from, date_to):
    result_set = []
    for item in d:
        for key, value in item['flat'].iteritems():
            if key=='created_at':
                a = dateparser.parse(value.replace("+0000", ""))
                date_from = dateparser.parse(str(date_from))
                date_to = dateparser.parse(str(date_to))
                if a > date_from and a < date_to:
                    result_set.append(item['original'])
    return result_set

# Due to shortage of time, I 
# am explaining what ill be 
# doing with the rest of the tasks
# for the rest of the tasks

# 1) for exact/partial matching strings, I would use regex and differentiate between exact or partial through a key in query paramters. 
#    Same can be done with starts with, endswiths
# 2) For integers, I would specify which key for integer, and check if it is integer, and use same search_by_key_value functionality
# 3) For sorting, same, specify key, and sort according to that, the flattened dict will help here
# 4) For grouping, I am not clear what is exactly required so I have left it out, but we can discuss it later on in the process once clear.

