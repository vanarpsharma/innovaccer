from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^search_tweets/$', views.search_tweets),
]
